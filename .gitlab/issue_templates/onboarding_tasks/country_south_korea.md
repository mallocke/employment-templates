### For team members located in South Korea

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Download the following documents from [Egnyte](https://globalupside.egnyte.com/app/index.do#storage/files/1/Shared/Clients/GU/Gitlab/Onboarding%20Documents) (Global Upside's online portal):
    - Employee Personal Particulars Form
    - Bank Details Form
    - Consent form 
    - Id/social security cert (front & back)
    - Bank letter
1. [ ] People Connect: Check all required payroll documents have been sent, completed and uploaded to Workday.
1. [ ] People Connect: In the team member's onboarding issue, ping the Non-US Payroll team member `@nprecilla` and `@sszepietowska` letting them know that the completed Payroll Questionnaire has been uploaded. 
1. [ ] People Connect: Verify that the new team member's Legal Name on their Photo ID matches the Legal Name entered into Workday.

</details>
