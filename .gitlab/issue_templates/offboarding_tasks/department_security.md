## FOR SECURITY ONLY

- [ ] @ankelly @dcouture: Remove the team member from HackerOne
- [ ] @ankelly @dcouture: Remove the team member from hackerone-customer.slack.com slack workspace
- [ ] @joe-dub @pmartinsgl: Remove the team member from Tenable.IO
- [ ] @laurence.bierner: Remove the team member from Rackspace (Security Enclave)
- [ ] @jfuentes2: Remove the team member from Panther SIEM
- [ ] @vmairet: Remove the team member from Tines
- [ ] @jburton: Remove the team member from Google Search Console
- [ ] @dfelton: Remove the team member from BitSight
- [ ] @dfelton: Remove the team member from Anecdotes
 


