### For team members in France

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Download the following documents from [Egnyte](https://globalupside.egnyte.com/app/index.do#storage/files/1/Shared/Clients/GU/Gitlab/Onboarding%20Documents) (Global Upside's online portal):
    - Employee Personal Particulars Form
    - Bank Details Form
    - Consent form 
    - Id/social security cert (front & back)
    - Bank letter

1. [ ] People Connect: Check the team members contract to see whether a 3 or 4 month probation period is applicable. Then, following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday (adjusting to 4 months if applicable).
1. [ ] People Connect: Confirm that all required payroll documents have been sent, completed and uploaded to Workday.
1. [ ] People Connect: In the team member's onboarding issue, ping the Non-US Payroll team member `@nprecilla` and `@sszepietowska` letting them know that the Payroll Questionnaire is completed and has been uploaded to Workday. 

</details>


