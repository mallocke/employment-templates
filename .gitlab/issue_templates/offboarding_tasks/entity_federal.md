## For GitLab Federal team members only

Jason Colyer, (US) Support Managers @jcolyer

1. [ ] Zendesk - US Federal

Brent Caldwell @bcaldwell-gitlab

1. [ ] GovWin IQ

Payroll @vlaughlan, @ybasha

1. [ ] Update team member status to ```Terminated``` in ADP.
1. [ ] Inactivate deductions under pay profile in ADP.
