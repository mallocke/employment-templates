### For team members in United Arab Emirates only


<details>
<summary>People Connect</summary>

1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.
1. [ ] People Connect: Check the team members nationality to see whether they are an expat to UAE. If yes, send an email to `ap@gitlab.com` with team members name and managers name in order to be added to the UAE TripActions policy (as part of benefits).
1. [ ] People Connect: Verify the new team member's Legal Name on their Photo ID matches the Legal Name entered in Workday.

</details>
