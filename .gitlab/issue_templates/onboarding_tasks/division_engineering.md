#### Engineering Division

<details>
<summary>New Team Member</summary>

1. [ ] Join the ['#engineering-fyi' channel](https://app.slack.com/client/T02592416/CJWA4E9UG), and make sure to read the Engineering Week-in-Review each week.
1. [ ] Check to see that you have been added to Engineering Google Group `engineering@gitlab.com`. Google Groups and members are reported [here](https://gitlab.com/gitlab-com/security-tools/report-gsuite-group-members/blob/master/engineering.csv).
    - Being a member of the Google Group should automatically add you to the recurring [CTO Office Hours](https://about.gitlab.com/handbook/engineering/#communication) event, which is also located on the [GitLab Team Meeting Calendar](https://about.gitlab.com/handbook/tools-and-tips/#gitlab-team-meetings-calendar).

</details>
