### Day 1 - For Team Members in India only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Read through the [India Specific Benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/global-upside-benefits-india/). This will explain what is available. If you have any questions please contact Global Upside at hr@globalpeoservices.com (Employment-related questions) or benefitsops@globalupside.com (Questions regarding benefits elections). You may also be contacted by Global Upside during your first week of starting at GitLab to complete their onboarding documents for payroll (if this has not already been done during the contract signing stage).
1. [ ] New team member: Global Upside will provide you with a declaration form on which you will indicate your desired Employees Provident Fund (EPF) contribution i.e. against assumed compensation or actual compensation.  It should be noted that Global Upside is not able to request or process a **reduction** in EPF contribution once your selection has been made.

</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.

</details>
