#### Professional Services Practice Engineer

<details>
<summary>New Team Member</summary>
_Welcome to the Practice Engineer Onboarding Experience! This issue will help learn about our current service offerings and ways of working._

:warning: If you find something that can be improved, please submit a merge request to this [issue template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/tree/main/.gitlab/issue_templates/onboarding_tasks/department_practice_management/role_practice_engineer.md) and send to the [#ps-practice](https://gitlab.slack.com/archives/C02DWMKHGRG) slack channel for review. 


## Section 1: Understanding our business, services and processes

_Weeks 2-3_
- [ ] Make sure you are enrolled in [Sales Quick Start (SQS)](https://about.gitlab.com/handbook/sales/onboarding/). If you are not, reach out to your manager to ensure you're scheduled. 
- [ ] Watch the latest high level professional services enablement [recording](https://gitlab.edcast.com/insights/sqs-22-professional). And/or check out the [associated slides](https://docs.google.com/presentation/d/1SIg3xYyFVQIPEe36VpsCJqt3sDp5boma4YpICRQMrEA/edit#slide=id.g75e3e400c6_6_123).
- [ ] Review the different types of [professional services offerings and delivery kits available](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/framework/)
- [ ] Learn about key details that tend to come up in [Migration Opportunities (25 mins)](https://www.loom.com/share/e02695d1092f476aaabdbd48e4c0a3f4?sharedAppSource=personal_library)
- [ ] Learn the fundamentals of [Distributied Systems Architecture (18 mins)](https://www.loom.com/share/6c6acebabce2474589eab0bea1879c2b?sharedAppSource=personal_library) to ensure you can speak the language required to sell/scope implementation services. 
    - [ ] Understand the basics of [Disaster Recovery (12 mins)](https://www.loom.com/share/535dddbe3eb247938fb2ba2e98eb625b?sharedAppSource=personal_library)
    - [ ] Learn about the [deployment Automation & Cloud Maturity (8 mins)](https://www.loom.com/share/9bd2745dbabb4384ac9a8aa369d79448?sharedAppSource=personal_library) 
- [ ] Review the different types of [education services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/#current-offerings) and when to position them.  
- [ ] Review the README files in the [Professional Services Delivery Kits](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits) to understand in more detail how we deliver services. 
    - [ ] Its important to note the `customer` foloder in the [Migration Template](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template) Project. There is lots of information about setting customer's expectations
    - [ ] The [migration customer documents](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template/-/tree/master/customer) are a great place to learn more about migration features and checklists that are discussed with the customer during pre-sales.
    - [ ] [Implementations](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/implementation-template)
    - [ ] [Readiness Assessments](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/readiness-assessment) (Health Checks)
    - [ ] Rapid Results ([SaaS](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/rapid-results-com)) ([self-managed](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/rapid-results-self-managed))
    

## Section 2: Certifications

_Weeks 2-6_

- [ ] Earn the GitLab [Certified Security Specialist](https://levelup.gitlab.com/courses/gitlab-certified-security-specialist) badge
- [ ] Earn the GitLab [Certified CI/CD Specialist](https://levelup.gitlab.com/courses/gitlab-ci-cd-specialist-self-paced-bundle) badge
- [ ] Earn the GitLab [Certified Migration Specialist](https://levelup.gitlab.com/courses/gitlab-certified-migration-specialist-bundle)
- [ ] Earn the GitLab [Certified Implementation Specialist](https://levelup.gitlab.com/courses/gitlab-certified-implementation-specialist-bundle)


## Section 3: DevSecOps Transformation 

_Weeks 2-4_

- [ ] Review the [DevSecOps Transformation Strategy](https://docs.google.com/document/d/1dJ5fiDtXwiCoL_zJGn8Tf0OSSe88Y37x6TCMvR26X5M/edit#heading=h.nvdytadepp1h).  
- [ ] Schedule 60 minutes or more with your manager to review the transformation proposal. 
- [ ] Understand how the vision of this service offering maps to [the roadmap](https://docs.google.com/spreadsheets/d/1YmVF6obcZQQ6JUX4EpcX1iJG8F6UrtMncSrULq3P8n8/edit#gid=0)
- Check out the [DevOps Transformation google drive folder](https://drive.google.com/drive/u/0/folders/1dxHhO_pO5Drykf5qMPzHVCg94RJZCnhB)
- [ ] Discuss with your Manager and fellow practice engineer(s) how to organize the backlog of work. 


## Section 4: Get to know the Code 

_Weeks 4-8_

- [ ] Install the following software:
  - [ ] [Homebrew](https://brew.sh/), package manager for MacOS
  - [ ] [Git](https://git-scm.com/). You should be able to install this through homebrew
  - [ ] [VS Code](https://code.visualstudio.com/), the default IDE for Professional Services
  - [ ] [Python 3.8](https://www.python.org/downloads/). You should be able to use Brew to handle this installation
  - [ ] [Poetry](https://python-poetry.org/), python package manager we use for most of our tooling
  - [ ] [NVM](https://github.com/nvm-sh/nvm) to handle different versions of Node/NPM if you need to do any frontend development
  - [ ] (Optional) [Pyenv](https://github.com/pyenv/pyenv) to handle switching between python versions
- [ ] [Congregate](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate) is the tool that GitLab PS uses for scaled source code migrations from the major competitors - Bitbucket and GitHub. 
    - [ ] Follow the [instructions](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/blob/master/docs/setup-dev-env.md) for setting up your dev environment. 
    - [ ] Add an MR to the instructions if you find anything thats confusing or wrong. 
- [ ] [Pipeline COE](https://gitlab.com/gitlab-org/professional-services-automation/pipelinecoe) is the framework that we're using to help customers build a standardized and governable CI/CD process. It will likely be a building block for the devsecops transformation service. 
    - [ ] understand the [value proposition (14 mins)](https://www.loom.com/share/da035d54cda74c039524d940766d7621?sharedAppSource=personal_library)
    - [ ] understand how the pipeline tempaltes and container templates work together. 
    - [ ] understand how we drop this framework of gitlab groups/projects into a customer's environment (their root group on gitlab.com or into their gitlab self managed instance)
    - [ ] schedule a chat with Chris Timberlake to ask questions about the technical delivery of the pipeline COE. 
- [ ] [Chameleon](https://gitlab.com/gitlab-com/customer-success/professional-services-group/global-practice-development/migration/chameleon) is a project that is the beginning of how we might automate the transition from legacy CI/CD orchestration systems to GitLab ci. It is a prototype/design right now.

## Section 5: Setup Test Environment

_Weeks 4-10_

- [ ] Set up an [AWS personal sandbox account](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/)
- [ ] Create [access requests](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#groupteam-aws-account-or-gcp-project-non-production) for the following AWS group sandboxes:
  - `sales-cs-sandbox-congregate`
  - `sales-cs-sandbox-proliferate`
- [ ] Check if you can access cloud.google.com through your GSuite account. If you cannot access any GCP resources, create a GCP account through the same Cloud Sandbox method above
- [ ] Jenkins & GitLab
  - [ ] You can use [this test Jenkins image](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/tree/master/docker/jenkins) to set up a Jenkins instance with some test data
  - [ ] Outside of the standard GitLab Omnibus package, we also have GitLab AMIs with test data baked into the image [here](https://us-east-1.console.aws.amazon.com/ec2/v2/home?region=us-east-1#Images:visibility=owned-by-me). You will need access to the congregate AWS sandbox before you can access these AMIs
- [ ] Add data, customize jenkins with many different use cases. 

## Section 6: Design, Planning and Ramp Exit 

_Weeks 6-12_

- [ ] Design a means to automate the transition from Jenkins to GitLab CI - leverage the Pipeline COE template framework. 
- [ ] Build a roadmap of issues to undertand level of effort to build.



