## For GitLab LTD employees only

<summary>People Connect</summary>

1. [ ] People Connect: Ping Non-US Payroll (@sszepietowska @nprecilla)


<summary>Total Rewards</summary>

1. [ ] Benefits Analyst (@julie.samson): Remove the team member from the medical plans by notifying WTW.
