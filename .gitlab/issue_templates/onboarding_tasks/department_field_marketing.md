#### Field Marketing

<details>
<summary>Manager</summary>

1. [ ] Please see the Field Marketing & Field Marketing Coordinator onboarding issue templates [here](https://gitlab.com/gitlab-com/marketing/onboarding) and open the appropriate one for your new team member. 
