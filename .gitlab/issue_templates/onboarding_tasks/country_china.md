### For team members in China

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Verify that the new team member's Legal Name on their Photo ID matches the Legal Name entered into Workday.

</details>
