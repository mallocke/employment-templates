### Manager of US Non-Exempt Team Member(s) 

We have added this section to your onboarding in the event that you either are set to manage one or more US non-exempt team members.

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please review GitLab's [Non-Exempt Policies & Processes](https://about.gitlab.com/handbook/finance/non-exempt/) handbook page.
    1. [ ] Please confirm that you understand whether your direct report(s)' role is Hourly non-exempt or Salaried non-exempt. If you are unsure, please review their Offer Letter and if you still have questions, please contact `people-compliance@gitlab.com`.
    1. [ ] New team member: Please confirm that you understand our [Meal and Rest Breaks](https://about.gitlab.com/handbook/finance/non-exempt/#meal-and-rest-breaks) policy.
    1. [ ] New team member: Please confirm that you understand our [Overtime](https://about.gitlab.com/handbook/finance/non-exempt/#overtime) policy and processes.
    1. [ ] New team member: Please confirm that you understand our [Commissions](https://about.gitlab.com/handbook/finance/non-exempt/#commissions) processes, if applicable to your direct report(s)' role and compensation plan.
    1. [ ] New team member: Please confirm that you are aware of the US non-exempt pay schedule.
    1. [ ] New team member: Complete the [ADP Time & Attendance Manager Training]() in LevelUp, even if you are not set to manage a US non-exempt team member immediately. This training is invaluable and will be given again should it be needed.

</details>

<details>
<summary>Payroll</summary>

1. [  ] Payroll @vlaughlan, @ybasha: Invite team member to ADP, as a manager of a US non-exempt team member.
1. [  ] Payroll @vlaughlan, @ybasha: Ensure team member has been given access to the Manager version of the ADP Time & Attendance module.

</details>
