### For Team Members in Finland

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Please ensure to upload a copy of your Passport and Bank Card/Banking Particulars to Workday, you can do so by navigating to your `Workday Profile` via your image in the upper right hand corner of the screen and selecting `Personal` followed by the `Documents Tab`. Be sure to notify People Connect that you have done so in the comments section of this issue.
1. [ ] Please complete the [Finland - New Employee Information Sheet](https://docs.google.com/spreadsheets/d/1tOYN9ylajqEr-Q_MmHs4Imu8j1QXuJ7V/edit#gid=1413931868) > click file, make a copy and save in your name (please ensure to duplicate otherwise your personal information can be obtained by other team members).

</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Send HR Savvy the following documentation (password protected) in order to process payroll and set up of team member
    - Signed contract
    - Copy of passport
    - Proof of bank card/details
1. [ ] People Connect: In the team member's onboarding issue, ping the Non-US Payroll team members `@nprecilla` and `@sszepietowska` letting them know that the Payroll form has been completed and in the Payroll Forms folder. 

</details>
