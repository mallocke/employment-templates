#### People Managers

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Review [Git page update](https://about.gitlab.com/handbook/git-page-update/) to learn more about contributing and using GitLab.
1. [ ] New team member: If applicable, review the [Vacancy Creation Process](https://about.gitlab.com/handbook/hiring/recruiting-framework/req-creation/), to learn how to open a new position.
1. [ ] New team member: Review the [Leadership handbook page](https://about.gitlab.com/handbook/leadership/), particularly the recommended [articles](https://about.gitlab.com/handbook/leadership/#articles) and [books](https://about.gitlab.com/handbook/leadership/#books).
1. [ ] New team member: Be sure to review our [gift guidelines](https://about.gitlab.com/handbook/people-group/celebrations/#significant-life-events) so you can request flowers or other items for your team members for appropriate occasions.
1. [ ] New team member: Be sure to complete Becoming A Manager training which has been automatically assigned to you. 

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: Give member `Maintainer` access on [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com).
1. [ ] Manager:  Before the new hire's first day, make a comment at the bottom of this issue if the new hire is taking over an existing team and provide the existing supervisory organization name or if they need a new supervisory organization created please share a unique name so one can be created. 
   * Once a supervisory organization is created you can move the appropriate workers by submitting a Job Change - Move Manager - in Workday
1. [ ] Manager: Email `ces@gitlab.com` if the new team member will be taking over existing need to fill positions and needs additional permissions in Greenhouse.
1. [ ] Manager: Submit an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to have the team member included in the below items (if applicable) and ping the IT Ops team in Slack if needed for urgent attention once created
   * `manager@` email alias
   * [Managers Google Group](https://groups.google.com/a/gitlab.com/g/managers/members)
   * Backend Managers: [`@gitlab-com/backend-managers`](https://gitlab.com/groups/gitlab-com/backend-managers/-/group_members?with_inherited_permissions=exclude) GitLab group
   * Frontend Managers: [`@gitlab-org/frontend/frontend-managers`](https://gitlab.com/groups/gitlab-org/frontend/frontend-managers/-/group_members?with_inherited_permissions=exclude) GitLab group
   * Infrastructure Managers: [`@gitlab-com/gl-infra/managers`](https://gitlab.com/groups/gitlab-com/gl-infra/managers/-/group_members?with_inherited_permissions=exclude) GitLab group
   * UX Managers: [`@gitlab-com/gitlab-ux/managers`](https://gitlab.com/groups/gitlab-com/gitlab-ux/managers/-/group_members?with_inherited_permissions=exclude)
   * Security Managers: [`@gitlab-com/gl-security/security-managers`](https://gitlab.com/groups/gitlab-com/gl-security/security-managers/-/group_members?with_inherited_permissions=exclude)
   * Support Managers: [`@gitlab-com/support/managers`](https://gitlab.com/groups/gitlab-com/support/managers/-/group_members?with_inherited_permissions=exclude) GitLab group

*Note: Not all people managers should be added to the groups above. This is managed on a role-specific and department/division specific basis. If you are unclear regarding which group(s) to add your team member to, please check with your manager.*

</details>


<details>
<summary>People Connect</summary>

1. [ ] People Connect: Check that an [Interview Training Issue](https://gitlab.com/gitlab-com/people-group/Training/blob/master/.gitlab/issue_templates/interview_training.md) has been automatically created and assigned to the team member in the [People Group Training project](https://gitlab.com/gitlab-com/people-group/Training/issues). Provide a link to the issue by commenting in the section below.
1. [ ] People Connect: Ensure that the [Becoming a GitLab Manager Issue](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md) has been automatically created and assigned to the team member in the [People Group Training Project](https://gitlab.com/gitlab-com/people-group/Training/issues). Provide a link to the issue by commenting in the section below.

</details>


<details>
<summary>Recruiting Operations</summary>

1. [ ] Recruiting Operations (@gl-recruitingops @MarissaFerber): Add team member to Greenhouse with `Basic` permissions. If the team member will be on a Hiring Team (e.g. an *Interviewer* or *Hiring Manager*), [upgrade their permissions](https://about.gitlab.com/handbook/hiring/greenhouse/#access-levels-and-permissions) to either `Interviewer` or `Job Admin: Hiring Manager` for the applicable Division or Department.

</details>
