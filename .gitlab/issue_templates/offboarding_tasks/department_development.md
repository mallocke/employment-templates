## FOR DEVELOPMENT ONLY

- [ ] @viktomas @andr3: Remove the team member from VS Code Azure
- [ ] @ankelly @dcouture Remove the team member from HackerOne, if applicable
- [ ] __MANAGER_HANDLE__: Remove from any [on-call obligations](https://docs.google.com/spreadsheets/d/10uI2GzqSvITdxC5djBo3RN34p8zFfxNASVnFlSh8faU/edit#gid=1066364624), and notify coordinator
- [ ] __MANAGER_HANDLE__: Remove [gitlab-license](https://rubygems.org/gems/gitlab-license/) ownership gem on RubyGems, if applicable. 
- [ ] If applicable, transfer ownership of [tracked repositories](https://about.gitlab.com/handbook/engineering/metrics/#projects-that-are-part-of-the-product), in the [projects CSV](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/data/projects_part_of_product.csv) to a new owner.
