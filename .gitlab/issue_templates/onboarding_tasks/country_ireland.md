### For team members in Ireland

<details>
<summary>People Connect</summary>


1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.

</details>

<details>

<summary>Team Member</summary>

1. [ ] Team Member: Review the [Ireland Benefits page](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/gitlab-ireland-ltd/) to enroll in medical, pension, etc. 

1. [ ] Team Member: [Complete Home Working Checklist](https://forms.gle/1U6nabJkYh8xQXdk7) within first 30 days of start date.

</details>

<details>

<summary>Total Rewards</summary>

1. [ ] Total Rewards @julie.samson: To enroll the new team member in Disability/Death in Service email Orca `employeebenefits@orca.ie` with the Name, Date of Birth, Gender, Occupation, Salary of the new team member. Ensure this personally identifiable data is password protected. 

</details>

<details>

<summary>Legal</summary>

1. [ ] @sarahrogers Review team member responses to home working checklist and follow up, if necessary.
</details>
