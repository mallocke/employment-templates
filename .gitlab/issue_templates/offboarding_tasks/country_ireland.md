### Total Rewards - Ireland Team Members

1. [ ] Total Rewards Analyst (@julie.samson): (Ireland team members only) Notify VHI (gemma.obrien@vhi.ie )(employeebenefits@orca.ie) about offboarding. Please make Orca aware prior to the team member leaving so Orca can arrange to contact them about their benefits before they leave. Orca requires the person’s date of leaving service and contact information.

