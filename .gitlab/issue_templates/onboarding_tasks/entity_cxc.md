## For employees with CXC only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: You should have been contacted already by CXC to walk you through their onboarding for payroll. Please reach out to People Operations if you have not received any information or have not been contacted by CXC.
1. [ ] New team member: Read through the [benefits section](https://about.gitlab.com/handbook/benefits/#CXC) for your location.

</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Ensure that the team members contract particulars have been captured in Workday you can add this information by navigating to the `Team Members Profile` > `Actions` > `Job Change` > `Add Contract` (job aid pending).
1. **Important:** For a 12-month contract with a March 4, 2019 start date, the end date would be March 3, 2020. Any following contract after this 12-month period would have a start date of March 4, 2020.
1. [ ] People Connect: Once the Legal Name in Workday has been checked, add a comment in the onboarding issue tagging the appropriate People Connect Associate (based on rotation issue). State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.
1. [ ] People Connect: Update the `Onboarding Audit::` label from `Missing` to `Waiting`

</details>
