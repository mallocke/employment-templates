### For team members in South Africa

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Refer to the South Africa benefits [page](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/remote-com/#south-africa) for a full overview of benefits currently offered in your region.
1. [ ] Important: Although Remote.com offers an option to submit expenses via their remote.com portal - please ensure that you **only** submit your expenses through GitLab (via [Expensify](https://about.gitlab.com/handbook/finance/expenses/#-introduction) - you get access to Expensify by day 2 of your onboarding).
1. [ ] Please also make sure to communicate your time off via [PTO by roots](https://about.gitlab.com/handbook/paid-time-off/#communicating-your-time-off) and not through the remote.com portal.

</details>

<details>
<summary>People Connect</summary>


1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.
1. [ ] People Connect: Verify that the new team member's Legal Name on their Photo ID matches the Legal Name entered into Workday.

</details>
