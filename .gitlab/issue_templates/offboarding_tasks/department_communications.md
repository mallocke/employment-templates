## FOR COMMUNICATIONS MEMBERS ONLY:

- [ ] @monicaj @sliang2: Remove access to Bananatag
- [ ] @wspillane: remove access to social channels: natively via Facebook, LinkedIn. Check tweetdeck and remove if needed. Be sure to cycle passwords in the social media admin vault on 1 Password

