Subject: Employment & Payroll Forms for GitLab GK (Japan)

Hi PERSON,

We are so excited to have you join GitLab! So that we can add you to our payroll system as quickly as possible, please complete the following documents:

- Employee Registration Form 
- Exemption for Dependent Form

Please let us know if you have any questions.

Thank you!
