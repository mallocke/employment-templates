## FOR FIELD OPERATIONS MEMBERS ONLY:

- [ ] @cfarris: Remove access to Vartopia
- [ ] @sheelaviswanathan: Remove access from Mail Distribution Group - `sfdcadmins@gitlab.com`
