### Day 1 - For Team Members in Belgium Only

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Be sure to upload your Vacation Certificate from your previous employer to Workday you can do so by navigating to your `Workday Profile` via your image in the upper right hand corner of the screen and selecting `Personal` followed by the `Documents Tab`. Be sure to notify People Connect that you have done so in the comments section of this issue to ensure accurate processing of your vacation pay in June.
</details>


<details>
<summary>People Connect</summary>

### Prior to Start Date:

1. [ ] People Connect: Open the [Belgium New Hire Form](https://docs.google.com/spreadsheets/d/1DEWQmhs4AVjRJUv_1L2iS8k7Ynl_IxBE/edit#gid=1440831444) in Google Drive. Select `make a copy` and save in the new team members name. Share the form with the team members personal email address as well as `nonuspayroll @gitlab.com`.
1. [ ] People Connect: Email the new team member the [Belgium email](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/email_templates/belgium_email.md) and cc `nonuspayroll @gitlab.com`.

### After Start Date: 

1. [ ] People Connect: Verify that the new team member's Legal Name on their Photo ID matches the Legal Name entered into Workday.

</details>
